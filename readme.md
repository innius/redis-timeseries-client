# redis-timeseries-client library

### Purpose
This library offers convenience functions for creating a connection with a redis-timeseries instance. 
Furthermore, it provides a function for constructing redis-keys based on the chosen convention.
Similarly, it provides a function for retrieving the retention scheme for each redis-key.

Ultimately, this library is to be used by any redis-timeseries client needing to ingest or query data.

### Use

##### Client
The library exposes multiple convenience functions for creating a redis-timeseries client. The generation of such a client ultimately requires a config.
This config can be either:
- provided directly
- stored in the secrets-manager

If the config is stored in the secrets-manager, the exposed functions can either take:
- a secret-ID directly
- an environment variable name that points to the secret-ID
- no arguments, in which case the function looks for an environment variable by the name of `REDIS_CONFIG_SECRET_ARN`

The client can be configured with or without a TLS connection. By default TLS will be enabled. To disable TLS, provide any of the above functions with the `WithInsecure` option.

##### Schema
The retention schema can be obtained using the `GetRetentionSchema`.

This will return the schema, including the aggregate-levels, rules, and supported aggregate-types.

* `Aggregate-level`: the accuracy of the value & the duration over which aggregates are calculated (e.g. raw, minute, 5-minute, hour)
* `Aggregate-type`: the type of aggregation (e.g. average, max, min, count)

##### Keys
The keys library exposes a function to construct rediskeys (`ConstructRedisKey`).
It also offers convenience functions for transforming an existing schema into its required with or without create-options (`Schema.ToRedisKeys` and `Schema.ToRedisKeysWithCreateOptions`). 

### Owner (who to blame)
Christiaan
Wolf