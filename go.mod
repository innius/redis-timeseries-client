module bitbucket.org/innius/redis-timeseries-client

go 1.17

require (
	bitbucket.org/innius/redistimeseries-go v0.4.1
	github.com/aws/aws-sdk-go v1.42.20
	github.com/gomodule/redigo v1.8.9
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
