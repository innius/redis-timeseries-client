package pkg

import (
	"os"
	"sort"
	"strconv"
	"time"

	rt "bitbucket.org/innius/redistimeseries-go"
)

type DataType string

const (
	Continuous DataType = "continuous"
	Discrete   DataType = "discrete"
	GPS        DataType = "gps"
)

type GetSchemaInput struct {
	DataType DataType
}

func supportsChanges() bool {
	supportsChanges, err := strconv.ParseBool(os.Getenv("SUPPORTS_CHANGES"))
	if err != nil || supportsChanges {
		return true
	}
	return false
}

func GetRetentionSchema(input GetSchemaInput) Schema {
	var schema Schema
	if input.DataType == Discrete {
		lvls := []AggregateLevel{
			{
				ID:        Raw,
				Retention: 3 * Year,
			},
		}
		if supportsChanges() {
			lvls = append(lvls, AggregateLevel{
				ID:            Changes,
				Retention:     3 * Year,
				IngestionRule: &AggregateRule{Raw, time.Second, []rt.AggregationType{rt.LastAggregation}},
			})
		}
		return Schema{DataType: input.DataType, AggregateLevels: lvls}
	}
	if input.DataType == GPS {
		lvls := []AggregateLevel{
			{
				ID:        Raw,
				Retention: 6 * Month,
			},
		}
		if supportsChanges() {
			lvls = append(lvls, AggregateLevel{
				ID:            Changes,
				Retention:     6 * Month,
				IngestionRule: &AggregateRule{Raw, time.Second, []rt.AggregationType{rt.LastAggregation}},
			})
		}
		return Schema{DataType: input.DataType, AggregateLevels: lvls}
	}
	if input.DataType == Continuous {
		lvls := []AggregateLevel{
			{
				ID:            Raw,
				Retention:     3 * Month,
				IngestionRule: nil,
			},
			{
				ID:            Minute,
				Retention:     1 * Year,
				IngestionRule: &AggregateRule{Raw, time.Minute, inniusAggregates},
			},
			{
				ID:            Minute5,
				Retention:     2 * Year,
				IngestionRule: &AggregateRule{Raw, 5 * time.Minute, inniusAggregates},
			},
			{
				ID:            Hour,
				Retention:     3 * Year,
				IngestionRule: &AggregateRule{Raw, time.Hour, inniusAggregates},
			},
		}

		if supportsChanges() {
			lvls = append(lvls, AggregateLevel{
				ID:            Changes,
				Retention:     3 * Month,
				IngestionRule: &AggregateRule{Raw, time.Second, []rt.AggregationType{rt.LastAggregation}},
			})
		}

		sort.Slice(lvls, func(i, j int) bool {
			return lvls[i].ID < lvls[j].ID
		})

		schema = Schema{
			DataType:        input.DataType,
			AggregateLevels: lvls,
		}

		return schema
	}

	return Schema{}
}

func (s Schema) IsAggregate(key AggregateLevelIdentifier) bool {
	for _, k := range s.AggregateLevels {
		if key == k.ID {
			return k.IngestionRule != nil
		}
	}
	return false
}
