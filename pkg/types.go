package pkg

import (
	"time"

	redistimeseries "bitbucket.org/innius/redistimeseries-go"
)

var inniusAggregates = []redistimeseries.AggregationType{
	redistimeseries.AvgAggregation,
	redistimeseries.MinAggregation,
	redistimeseries.MaxAggregation,
}

type AggregateLevelIdentifier int

const (
	Day     = 24 * time.Hour
	Week    = 7 * Day
	Month   = 31 * Day
	Quarter = 3 * Month
	Year    = 4 * Quarter

	Raw AggregateLevelIdentifier = iota
	Minute
	Minute5
	Hour
	Changes

	Label_Rate             string = "rate"
	Label_Sensor_Data_Type string = "sensor_data_type"
)

func (a AggregateLevelIdentifier) IsCopyKey() bool {
	if a == Changes {
		return true
	}
	return false
}

func (a AggregateLevelIdentifier) String() string {
	switch a {
	case Raw:
		return ""
	case Minute:
		return "minute"
	case Minute5:
		return "minute5"
	case Changes:
		return "changes"
	default:
		return "hour"
	}
}

type AggregateLevel struct {
	ID            AggregateLevelIdentifier
	Retention     time.Duration
	IngestionRule *AggregateRule
}

type Schema struct {
	DataType        DataType
	AggregateLevels []AggregateLevel
}

type AggregateRule struct {
	Source         AggregateLevelIdentifier
	BucketSize     time.Duration
	AggregateTypes []redistimeseries.AggregationType
}

type ClientConfig struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
	Auth string `yaml:"auth"`
	// Maximum number of idle connections in the pool.
	PoolMaxIdle int `yaml:"pool_max_idle"`
	// Close connections after remaining idle for this duration.
	PoolIdleTimeout time.Duration `yaml:"pool_idle_timeout"`
	// Maximum number of connections allocated by the pool at a given time.
	PoolMaxActive int `yaml:"pool_max_active"`
	TlsConfig     struct {
		InsecureSkipVerify bool   `yaml:"insecure_skip_verify"`
		Ca                 string `yaml:"ca"`
		Cert               string `yaml:"crt"`
		Key                string `yaml:"key"`
	} `yaml:"tls_config"`
}
