package pkg

import (
	"strconv"
	"strings"

	redistimeseries "bitbucket.org/innius/redistimeseries-go"
)

func ConstructRedisKey(machineTrn, sensorID string, aggregateID AggregateLevelIdentifier, aggregationType *redistimeseries.AggregationType) string {
	keyElems := []string{machineTrn, sensorID}
	if aggregateID.IsCopyKey() {
		e := append(keyElems, aggregateID.String())
		return strings.Join(e, ":")
	}
	if aggregateID.String() != "" && aggregationType != nil {
		keyElems = append(keyElems, aggregateID.String(), string(*aggregationType))
	}
	return strings.Join(keyElems, ":")
}

// convenience function for creating new redis-keys
func (s Schema) ToRedisKeysWithCreateOptions(machineID, sensorID string, sensorRate int) map[string]redistimeseries.CreateOptions {
	keys := map[string]redistimeseries.CreateOptions{}

	for _, level := range s.AggregateLevels {
		if s.IsAggregate(level.ID) {
			for _, a := range level.IngestionRule.AggregateTypes {
				keys[ConstructRedisKey(machineID, sensorID, level.ID, &a)] = redistimeseries.CreateOptions{
					RetentionMSecs:  level.Retention,
					DuplicatePolicy: redistimeseries.LastDuplicatePolicy,
				}
			}
		} else {
			keys[ConstructRedisKey(machineID, sensorID, level.ID, nil)] = redistimeseries.CreateOptions{
				RetentionMSecs:  level.Retention,
				DuplicatePolicy: redistimeseries.LastDuplicatePolicy,
				Labels: map[string]string{
					Label_Rate:             strconv.Itoa(sensorRate),
					Label_Sensor_Data_Type: string(s.DataType),
				},
			}
		}
	}
	return keys
}

// convenience function for listing all redis-keys (e.g. for deletion)
func (s Schema) ToRedisKeys(machineID, sensorID string) []string {
	keys := []string{}

	for _, level := range s.AggregateLevels {
		if s.IsAggregate(level.ID) {
			for _, a := range level.IngestionRule.AggregateTypes {
				keys = append(keys, ConstructRedisKey(machineID, sensorID, level.ID, &a))
			}
		} else {
			keys = append(keys, ConstructRedisKey(machineID, sensorID, level.ID, nil))
		}
	}
	return keys
}
