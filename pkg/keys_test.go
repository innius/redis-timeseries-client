package pkg

import (
	"testing"
	"time"

	redistimeseries "bitbucket.org/innius/redistimeseries-go"
	"github.com/stretchr/testify/assert"
)

var avg = redistimeseries.AvgAggregation
var min = redistimeseries.MinAggregation

func TestKeys(t *testing.T) {
	t.Run("schema.to-keys funcs should ", func(t *testing.T) {
		machineID := "machineID"
		sensorID := "sensorID"

		aggTypes := []redistimeseries.AggregationType{
			redistimeseries.AvgAggregation,
			redistimeseries.MinAggregation,
		}

		var schema = Schema{
			DataType: Discrete,
			AggregateLevels: []AggregateLevel{
				{
					ID:        Raw,
					Retention: time.Hour,
				}, {
					ID:            Minute,
					Retention:     2 * time.Hour,
					IngestionRule: &AggregateRule{Raw, time.Minute, aggTypes},
				}, {
					ID:            Minute5,
					Retention:     5 * time.Hour,
					IngestionRule: &AggregateRule{Raw, 5 * time.Minute, aggTypes},
				},
			},
		}
		t.Run("construct all & correct redis-keys as string", func(t *testing.T) {
			expected := []string{
				ConstructRedisKey(machineID, sensorID, Raw, nil),
				ConstructRedisKey(machineID, sensorID, Minute, &avg),
				ConstructRedisKey(machineID, sensorID, Minute, &min),
				ConstructRedisKey(machineID, sensorID, Minute5, &avg),
				ConstructRedisKey(machineID, sensorID, Minute5, &min),
			}
			res := schema.ToRedisKeys(machineID, sensorID)
			assert.EqualValues(t, res, expected)
		})

		t.Run("create redis-keys with the correct create options", func(t *testing.T) {
			keys := []string{
				ConstructRedisKey(machineID, sensorID, Raw, nil),
				ConstructRedisKey(machineID, sensorID, Minute, &avg),
				ConstructRedisKey(machineID, sensorID, Minute, &min),
				ConstructRedisKey(machineID, sensorID, Minute5, &avg),
				ConstructRedisKey(machineID, sensorID, Minute5, &min),
			}
			expected := map[string]redistimeseries.CreateOptions{
				keys[0]: {
					RetentionMSecs:  time.Hour,
					DuplicatePolicy: redistimeseries.LastDuplicatePolicy,
					Labels: map[string]string{
						Label_Rate:             "3600",
						Label_Sensor_Data_Type: "discrete",
					},
				},
				keys[1]: {
					RetentionMSecs:  2 * time.Hour,
					DuplicatePolicy: redistimeseries.LastDuplicatePolicy,
				},
				keys[2]: {
					RetentionMSecs:  2 * time.Hour,
					DuplicatePolicy: redistimeseries.LastDuplicatePolicy,
				},
				keys[3]: {
					RetentionMSecs:  5 * time.Hour,
					DuplicatePolicy: redistimeseries.LastDuplicatePolicy,
				},
				keys[4]: {
					RetentionMSecs:  5 * time.Hour,
					DuplicatePolicy: redistimeseries.LastDuplicatePolicy,
				},
			}

			res := schema.ToRedisKeysWithCreateOptions(machineID, sensorID, 3600)
			assert.EqualValues(t, expected, res)
		})
	})
}
