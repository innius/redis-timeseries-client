package pkg

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"net"
	"os"
	"time"

	"gopkg.in/yaml.v3"

	redistimeseries "bitbucket.org/innius/redistimeseries-go"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/gomodule/redigo/redis"
)

// convenience function for retrieving redis-timeseries client directly from provided config
func NewClientFromConfig(config ClientConfig) (*redistimeseries.Client, error) {
	address := net.JoinHostPort(config.Host, fmt.Sprintf("%d", config.Port))
	cert, err := tls.X509KeyPair([]byte(config.TlsConfig.Cert), []byte(config.TlsConfig.Key))
	if err != nil {
		panic(fmt.Sprintf("error creating Aggregate pair: %v", err))
	}

	tlsConf := &tls.Config{
		InsecureSkipVerify: config.TlsConfig.InsecureSkipVerify,
	}
	tlsConf.Certificates = []tls.Certificate{cert}

	roots := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM([]byte(config.TlsConfig.Ca))
	if !ok {
		panic("failed to parse root certificate")
	}
	tlsConf.RootCAs = roots

	pool := newPool(config, func() (redis.Conn, error) {
		return redis.Dial("tcp", address, redis.DialUseTLS(true), redis.DialTLSConfig(tlsConf), redis.DialPassword(config.Auth))
	})

	return redistimeseries.NewClientFromPool(pool, "connector"), nil
}

func newPool(config ClientConfig, dialFunc func() (redis.Conn, error)) *redis.Pool {
	maxActive := 500
	if config.PoolMaxActive > 0 {
		maxActive = config.PoolMaxActive
	}
	maxIdle := 500
	if config.PoolMaxIdle > 0 {
		maxIdle = config.PoolMaxIdle
	}
	idleTimeout := 10 * time.Second
	if config.PoolIdleTimeout > 0 {
		idleTimeout = config.PoolIdleTimeout
	}

	return &redis.Pool{
		TestOnBorrow: testOnBorrow,
		IdleTimeout:  idleTimeout,
		MaxIdle:      maxIdle,
		MaxActive:    maxActive,
		Dial:         dialFunc,
	}
}

func testOnBorrow(c redis.Conn, t time.Time) (err error) {
	if time.Since(t) > time.Millisecond {
		_, err = c.Do("PING")
	}
	return err
}

// convenience function for reading client config from AWS secretsmanager using a secret-ID stored in the REDIS_CONFIG_SECRET_ARN environment variable
func NewClientFromEnv(options ...Option) (*redistimeseries.Client, error) {
	return NewClientFromEnvVar("REDIS_CONFIG_SECRET_ARN", options...)
}

// convenience function for reading client config from AWS secretsmanager using a secret-ID stored in the given environment variable
func NewClientFromEnvVar(envVar string, options ...Option) (*redistimeseries.Client, error) {
	SecretId, ok := os.LookupEnv(envVar)
	if !ok {
		return nil, errors.New("could not find redis secret arn in environment!")
	}
	return NewClientFromSecretID(SecretId, options...)
}

// convenience function for generating client from secretString, e.g. in a local session
func NewClientFromSecretString(SecretString string) (*redistimeseries.Client, error) {
	cfg, err := parseConfig(aws.StringValue(&SecretString))
	if err != nil {
		return nil, err
	}

	return NewClientFromConfig(cfg)
}

// convenience function for reading client config from AWS secretsmanager using the given secret-ID
func NewClientFromSecretID(SecretId string, options ...Option) (*redistimeseries.Client, error) {
	cfg := config{
		session: session.Must(session.NewSession()),
	}

	for _, opt := range options {
		opt(&cfg)
	}

	secretsManager := secretsmanager.New(cfg.session)
	res, err := secretsManager.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId:     aws.String(SecretId),
		VersionId:    nil,
		VersionStage: nil,
	})
	if err != nil {
		return nil, err
	}
	return NewClientFromSecretString(*res.SecretString)
}

type config struct {
	session *session.Session
}

type Option func(*config)

func WithSession(sess *session.Session) Option {
	return func(c *config) {
		c.session = sess
	}
}

func parseConfig(data string) (ClientConfig, error) {
	t := ClientConfig{}
	err := yaml.Unmarshal([]byte(data), &t)
	return t, err
}
